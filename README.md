## About

Chromium-based web browser that uses the PySide6 module.  
Imposing, robust, focused on protecting your privacy. Surface, onion and i2p network support.  
Also, by default, all anti-privacy settings are disabled. It does not support Sec-CH-UA.

### Installing the necessary programs (most of it will probably already be included in your Linux distribution)

- On Debian, Kali, Ubuntu, Mint:
```bash
xargs -a programs.txt sudo apt install -y
```

- On Arch, Manjaro:
```bash
cat programs.txt | xargs sudo pacman -S --noconfirm
```

- On Red Hat, CentOS, Fedora:
```bash
sudo dnf install -y $(cat programs.txt)
```

## Install Python Modules
- On Debian Linux:

```bash
$ python3.11 -m pip install --break-system-packages --no-warn-script-location --user -r requirements.txt
```

- Other Linux:

```bash
$ python3.11 -m pip install --user -r requirements.txt
```

### How to use:

```bash
chmod +x browser.py
```

```bash
./browser.py
```

### Screenshots
| home | settings |
|------------------------------------------------------|---|
| ![ Photo of stream ]( index.png  "title" ) | ![ Photo of stream ]( settings.png  "title" ) |


### Shortcuts
    ctrl+q                Close the browser  
    ctrl+x                Close the browser  
    ctrl+u                View page source  
    ctrl+y                View page source in new window  
    ctrl+z                Close page source in new window  
    esc                   Close page source in new window  
    ctrl+Key_Left         Back_shortcut  
    ctrl+Key_Right        Forward_shortcut  
    ctrl+r                Rem tab shortcut  
    ctrl+n                Add tab shortcut  
    ctrl+s                Refresh shortcut  

