#!/usr/bin/env python3.11

import os
import re
import sys
import time
import shutil
import signal
#import asyncio
import tempfile
import functools
import threading
import subprocess
import concurrent.futures
from functools import partial
from datetime import datetime
from QtWebEngine.Load.get_ip import GetIP
from QtWebEngine.Load.speed_test import SpeedTest
from QtWebEngine.Load.user_agent import UserAgents
from QtWebEngine.Load.onion_check import OnionCheck
from QtWebEngine.Load.search_engine import SEARCH_ENGINES
from QtWebEngine.Load.search_engine_onion import SEARCH_ENGINES_ONION
from QtWebEngine.Load.search_engine_i2p import SEARCH_ENGINES_I2P
from QtWebEngine.Load.download_manager import DownloadManager
from QtWebEngine.Load.book import fixed, bookmarks, onion, i2p, shortcuts, user_agent, user_agent_
try:
    import requests
    from urllib.parse import urlparse
    from urllib.parse import urlencode
except ImportError:
    sys.exit("""Please install missing librarys or dependencies:
        python3.11 -m pip install --break-system-package --user requests
        python3.11 -m pip install --break-system-package --upgrade requests urllib3 chardet charset_normalizer """)
try:
    import argparse
    from argparse import RawTextHelpFormatter
except ImportError:
    sys.exit("""Please install missing librarys or dependencies:
        python3.11 -m pip install --break-system-package --user argparse
        """)
try:
    from PySide6.QtCore import QUrl, Qt, QSize, QTimer, Slot, QEvent, Signal, QThread, QObject, QCoreApplication
    from PySide6.QtGui import QKeySequence, QShortcut, QAction, QIcon
    from PySide6.QtWidgets import QApplication, QMainWindow, QToolBar, QLineEdit, QPushButton, QTextEdit, QVBoxLayout, QWidget, QMenu, QTabWidget, QTabBar, QCheckBox, QLabel, QProgressBar, QScrollArea, QHBoxLayout, QStatusBar, QSpacerItem, QSizePolicy, QMessageBox, QFileDialog, QDialog, QInputDialog, QMenuBar, QComboBox, QDialogButtonBox
    from PySide6.QtWebEngineWidgets import QWebEngineView
    from PySide6.QtWebEngineCore import QWebEngineSettings, QWebEngineDownloadRequest, QWebEnginePage
    from PySide6.QtNetwork import QNetworkProxy, QNetworkRequest
except ImportError:
    sys.exit("""Please install missing librarys and dependencies:
        apt install qtbase5-dev libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-render-util0 libxcb-cursor0 libxcb-xinerama0 '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev -y
        python3.11 -m pip install --break-system-package --user PySide6
        python3.11 -m pip install --break-system-package --user PyQtWebEngine
        """)
#python3.11 -m pip install --break-system-package PySide6.QtNetwork
#Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user pysocks"

TITLE = "Z Browser: Robust, private and imposing web browser"
VERSION = "3.0.3.1"
DATE = "30 nov 2023"
AUTOR = "zeyerq"

print(f"""
zzzzzzzzzzzzzzzzz
zzzzzzzzzzzzzzzzz
zzzzzzzzzzzzzzzzz
        zzzzzzzz
       zzzzzzz
     zzzzzzzz
   zzzzzzzz
 zzzzzzzz
zzzzzzzzzzzzzzzz
zzzzzzzzzzzzzzzz
zzzzzzzzzzzzzzzz

Created by {AUTOR}
VERSION: {VERSION}\nDATE: {DATE}
""")

css_dir = os.path.dirname(os.path.realpath(__file__))
with open(f'{css_dir}/QtWebEngine/Load/styles.css', 'r') as file:
    css_style = file.read()

class UserAgentDialog(QDialog):
    def __init__(self, parent=None):
        super(UserAgentDialog, self).__init__(parent)

        self.setWindowTitle("Choose User Agent")

        layout = QVBoxLayout()

        label = QLabel("Select a User Agent:")
        self.user_agent_combobox = QComboBox()

        self.user_agent_combobox.addItem("Windows 11", UserAgents.WINDOWS_11)
        self.user_agent_combobox.addItem("Chrome (Linux)", UserAgents.CHROME_LINUX)
        self.user_agent_combobox.addItem("Chrome on Windows 10", UserAgents.CHROME_WIN10_MOD1)
        self.user_agent_combobox.addItem("Chrome on Windows 10", UserAgents.CHROME_WIN10_MOD2)
        self.user_agent_combobox.addItem("Chrome on Windows 10", UserAgents.CHROME_WIN10_MOD3)
        self.user_agent_combobox.addItem("Firefox 114 windows", UserAgents.FIREFOX_114_WINDOWS)
        self.user_agent_combobox.addItem("Chrome on Iphone", UserAgents.CHROME_ON_IPHONE)
        self.user_agent_combobox.addItem("Chrome on Ipad", UserAgents.CHROME_ON_IPAD)
        self.user_agent_combobox.addItem("Chrome on Ipod", UserAgents.CHROME_ON_IPOD)
        self.user_agent_combobox.addItem("Chrome (Standard) iOS", UserAgents.CHROME_STANDART_IOS)
        self.user_agent_combobox.addItem("Chrome on Samsung SM-A205U", UserAgents.CHROME_ON_SANSUNG_SM_A205U)
        self.user_agent_combobox.addItem("Chrome on Samsung SM-A102U", UserAgents.CHROME_ON_SANSUNG_SM_A102U)
        self.user_agent_combobox.addItem("Chrome on Samsung SM-G960U", UserAgents.CHROME_ON_SANSUNG_SM_G960U)
        self.user_agent_combobox.addItem("Chrome on Samsung SM-N960U", UserAgents.CHROME_ON_SANSUNG_SM_N960U)
        self.user_agent_combobox.addItem("Chrome on Lg LM-Q720", UserAgents.CHROME_ON_lG_LM_Q720)
        self.user_agent_combobox.addItem("Chrome on Lg LM-X420", UserAgents.CHROME_ON_lG_LM_X420)
        self.user_agent_combobox.addItem("Chrome on Lg LM-Q710(FGN)", UserAgents.CHROME_ON_lG_LM_Q710_FGN)
        self.user_agent_combobox.addItem("Googlebot Desktop", UserAgents.GOOGLEBOT_DESKTOP)

        layout.addWidget(label)
        layout.addWidget(self.user_agent_combobox)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        layout.addWidget(button_box)

        self.setLayout(layout)

    def get_selected_user_agent(self):
        return self.user_agent_combobox.currentData()


class CloseableTabBar(QTabBar):
    def __init__(self):
        super().__init__()

    def closeTab(self, index):
        self.tabCloseRequested.emit(index)


class TabbedBrowser(QTabWidget):
    def __init__(self, args):
        super().__init__()
        self.downloads = {} # Dictionary to track download status

        rem_tab_shortcut = QShortcut(QKeySequence("Ctrl+Z"), self)
        rem_tab_shortcut.activated.connect(self.close_current_tab)

        self.toggle_images_state = True if args.enable_images else False
        self.toggle_javascript_state = True if args.enable_javascript or args.only_verified_certificates else False
        self.toggle_popup_state = True if args.enable_popup else False
        self.toggle_cookies_state = True if args.enable_cookies else False
        self.toggle_error_state = True if args.enable_error or args.only_verified_certificates else False
        self.toggle_storage_state = True if args.enable_local_storage else False
        self.toggle_content_state = True if args.enable_local_remote_acess else False
        self.toggle_webgl_state = True if args.enable_webgl else False
        self.toggle_plugins_state = True if args.enable_plugins else False
        self.toggle_screen_state = True if args.enable_screen_capture else False
        self.toggle_dns_prefetch_state = True if args.enable_dns_prefetch else False
        self.toggle_webrtc_state = False if args.disable_webrtc else True
        self.toggle_proxy_socks_state = True if args.proxy else False
        self.toggle_proxy_i2p_state = True if args.proxy else False
        self.toggle_proxy_i2p_https_state = True if args.proxy else False
        self.togle_disable_all_state = False
        self.togle_enable_all_state = False
        self.toggle_check_state = True if args.footer_check else False
        self.toggle_links_state = True if args.footer_link else False
        self.toggle_download_confirmation_state = True if args.download_confirmation else False
        self.tabs = []

        self.user_agent = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'

        if self.toggle_check_state:
            state = False
            self.toggle_check(state)

#        self.tabBar = CloseableTabBar()
#        self.setTabBar(self.tabBar)
#        self.tabBar.tabCloseRequested.connect(self.removeTab)
        self.addTab(self.create_new_tab(args), "Tab 1")

        add_tab_button = QPushButton("+")
        add_tab_button.clicked.connect(lambda: self.add_new_tab(args))
        self.setCornerWidget(add_tab_button, Qt.TopLeftCorner)
        add_tab_button.setStyleSheet("background-color: #000000; color: #0EAEF8; text-align: center; font-size: 30px; border: None; font-weight: bold;")
        rem_tab_button = QPushButton("QUIT")
        rem_tab_button.clicked.connect(lambda: self.main_signal())
        self.setCornerWidget(rem_tab_button, Qt.TopRightCorner)
        rem_tab_button.setStyleSheet("background-color: #000000; color: #ff0000; text-align: center; font-size: 14px; border: None; font-weight: bold;")

        self.tabs = [self.currentWidget()]

    def close_current_tab(self):
        current_tab_index = self.indexOf(self.currentWidget())
        if current_tab_index != -1:
            self.removeTab(current_tab_index)
            if self.count() == 0:
                self.close()

    def add_new_tab(self, args, url=None):
        self.toggle_links_state = False
        self.toggle_check_state = False
        new_tab_index = self.count() + 1
        tab = self.create_new_tab(args)
        self.addTab(tab, f"Tab {new_tab_index}")
        self.tabs.append(tab)
        self.setCurrentIndex(new_tab_index - 1)

        if url:
            tab.browser.setUrl(QUrl(url))

        tab.browser.page().profile().setHttpUserAgent(self.user_agent)
        return tab

    def create_new_tab(self, args):
        tab = BrowserWindow(args, self)

        tab.browser.titleChanged.connect(self.update_tab_title)
        tab.browser.settings().setAttribute(QWebEngineSettings.AutoLoadImages, self.toggle_images_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, self.toggle_javascript_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.JavascriptCanAccessClipboard, self.toggle_javascript_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, self.toggle_popup_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.ErrorPageEnabled, self.toggle_error_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.LocalStorageEnabled, self.toggle_storage_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.LocalContentCanAccessRemoteUrls, self.toggle_content_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.WebGLEnabled, self.toggle_webgl_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.PluginsEnabled, self.toggle_plugins_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.ScreenCaptureEnabled, self.toggle_screen_state)
        tab.browser.settings().setAttribute(QWebEngineSettings.DnsPrefetchEnabled, self.toggle_dns_prefetch_state)
        tab.browser.page().profile().settings().setAttribute(QWebEngineSettings.WebRTCPublicInterfacesOnly, self.toggle_webrtc_state)
        self.toggle_proxy_socks_state
        self.toggle_proxy_i2p_state
        self.toggle_proxy_i2p_https_state
        self.togle_disable_all_state
        self.togle_enable_all_state
        self.toggle_check_state
        self.toggle_links_state
        self.toggle_download_confirmation_state
        self.tabs.append(tab)

        if self.toggle_cookies_state:
            os.environ['QTWEBENGINE_COOKIE_STORE'] = '1'
        else:
            os.environ['QTWEBENGINE_COOKIE_STORE'] = '0'

        if self.toggle_proxy_socks_state:
            proxy = QNetworkProxy()
            if args.proxy:
                proxy_type, proxy_addr = args.proxy.split("://")
                proxy.setType(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.HttpProxy)
                proxy.setHostName(proxy_addr.split(":")[0])
                proxy.setPort(int(proxy_addr.split(":")[1]))
            else:
                proxy.setType(QNetworkProxy.Socks5Proxy)
                proxy.setHostName("127.0.0.1")
                proxy.setPort(9050)
            QNetworkProxy.setApplicationProxy(proxy)

        if self.toggle_proxy_i2p_state:
            proxy = QNetworkProxy()
            if args.proxy:
                proxy_type, proxy_addr = args.proxy.split("://")
                proxy.setType(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.HttpProxy)
                proxy.setHostName(proxy_addr.split(":")[0])
                proxy.setPort(int(proxy_addr.split(":")[1]))
            else:
                proxy.setType(QNetworkProxy.HttpProxy)
                proxy.setHostName("127.0.0.1")
                proxy.setPort(4444)
            QNetworkProxy.setApplicationProxy(proxy)

        if self.toggle_proxy_i2p_https_state:
            proxy = QNetworkProxy()
            if args.proxy:
                proxy_type, proxy_addr = args.proxy.split("://")
                proxy.setType(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.HttpProxy)
                proxy.setHostName(proxy_addr.split(":")[0])
                proxy.setPort(int(proxy_addr.split(":")[1]))
            else:
                proxy.setType(QNetworkProxy.HttpProxy)
                proxy.setHostName("127.0.0.1")
                proxy.setPort(4445)
            QNetworkProxy.setApplicationProxy(proxy)

        if self.togle_disable_all_state:
            self.toggle_images_state = False
            self.toggle_javascript_state = False
            self.toggle_popup_state = False
            self.toggle_cookies_state = False
            self.toggle_error_state = False
            self.toggle_storage_state = False
            self.toggle_content_state = False
            self.toggle_webgl_state = False
            self.toggle_plugins_state = False
            self.toggle_screen_state = False
            self.toggle_dns_prefetch_state = False
            self.toggle_proxy_socks_state = False

        if self.togle_enable_all_state:
            self.toggle_images_state = True
            self.toggle_javascript_state = True
            self.toggle_popup_state = True
            self.toggle_cookies_state = True
            self.toggle_error_state = True
            self.toggle_storage_state = True
            self.toggle_content_state = True
            self.toggle_webgl_state = True
            self.toggle_plugins_state = True
            self.toggle_screen_state = True
            self.toggle_dns_prefetch_state = True

        if self.toggle_check_state:
            state = True
            self.toggle_check(state)

        if self.toggle_links_state:
            state = True
            self.toggle_links(state)

        if self.toggle_download_confirmation_state:
            state = True
            self.toggle_download_confirmation(state)

        tab.browser.page().profile().setHttpUserAgent(self.user_agent)
        return tab

    def update_tab_title(self):
        current_tab_index = self.indexOf(self.currentWidget())
        if current_tab_index != -1:
            current_widget = self.currentWidget()

            try:
                if current_widget.browser and current_widget.browser.page():
                    title = current_widget.browser.page().title()
                else:
                    title = None
            except:
                title = None

            if title:
                title = title[:25] if len(title) > 25 else title
            else:
                title = "Loading..."

            if len(title) == 0:
                title = "New Tab"
            self.setTabText(current_tab_index, title)

    def open_settings(self):
        self.create_settings_tab()

    def open_historic(self):
        self.create_historic_tab()

    def open_downloads_settings(self):
        self.download_manager()

    def open_informations(self, state):
        self.create_information_tab(state)

    def download_manager(self):
        for index in range(self.count()):
            if self.tabText(index) == "Downloads":
                self.setCurrentIndex(index)
                return

        downloads_tab = QMainWindow()
        downloads_tab.setWindowTitle("Downloads")

        scroll_area = QScrollArea()
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        downloads_tab.setCentralWidget(scroll_area)

        downloads_widget = QWidget()
        scroll_area.setWidget(downloads_widget)
        scroll_area.setWidgetResizable(True)

        self.addTab(downloads_tab, "Downloads")
        self.setCurrentIndex(self.count() - 1)

        download_directory = "/tmp"

        download_label = QPushButton("Download Manager")
        download_label.setStyleSheet("background-color: #000000; color: #0EAEF8; text-align: center; font-size: 28px; border: None;")
        download_directory_label = QPushButton(f"Download directory: {args.download_directory if args.download_directory else download_directory}")
        download_directory_label.setStyleSheet("background-color: #000000; color: #0EAEF8; text-align: left; font-size: 18px; border: None;")

        layout = QVBoxLayout()
        layout.addWidget(download_label)
        layout.addWidget(download_directory_label)

#        progress_button = QPushButton("Show Progress")
#        progress_button.setStyleSheet("background-color: #000000; color: #ffff00; font-size: 18px; border: None;")
#        progress_button.clicked.connect(self.show_progress)
#        layout.addWidget(progress_button)
#        downloads_widget.setLayout(layout)

#        cancel_button = QPushButton("Cancel download")
#        cancel_button.setStyleSheet("background-color: #000000; color: #ff0000; font-size: 18px; border: None;")
#        cancel_button.clicked.connect(self.cancel_download())
#        layout.addWidget(cancel_button)
#        downloads_widget.setLayout(layout)

# QLabel widget to show file name in progress
        self.download_name_label = QLabel()
        layout.addWidget(self.download_name_label)
        if self.downloads:
            download_text = "\n".join([f"{self.downloads[name]['status']}: {self.downloads[name]['file_name']}" for name in self.downloads])
            self.download_name_label.setText(download_text)
        else:
            self.download_name_label.setText("")

        exit_button = QPushButton("EXIT (Ctrl+z)")
        exit_button.setStyleSheet("background-color: #000000; color: red; font-size: 28px; border: None;")
        exit_button.clicked.connect(self.close_current_tab)
        layout.addWidget(exit_button)
        downloads_widget.setLayout(layout)

    def cancel_download(self):
        pass

    def show_progress(self):
        WebEngineView.show_progress_flag = True

    def set_download_name(self, name, status="Downloading"):
        self.download_manager()
# Add the file name to the download dictionary
        self.downloads[name] = {"status": status, "file_name": name}
# Update QLabel text to show all downloads
        download_text = "\n".join([f"{self.downloads[name]['status']}: {self.downloads[name]['file_name']}" for name in self.downloads])
        self.download_name_label.setText(download_text)

    def set_download_progress(self, name, progress):
        if name in self.downloads:
            self.downloads[name]["status"] = f"Downloading ({progress})%"
            self.set_download_name(name, f"Downloading ({progress})%")

    def set_download_finished(self, name):
        if name in self.downloads:
            self.downloads[name]["status"] = "Finished"
            self.set_download_name(name, "Finished")

    def create_settings_tab(self):
# Check if a tab named "Settings" already exists
        for index in range(self.count()):
            if self.tabText(index) == "Settings":
# If it exists, bring the existing tab to the front
                self.setCurrentIndex(index)
                return

# If it doesn't exist, create a new settings tab
        settings_tab = QMainWindow()
        settings_tab.setWindowTitle("Settings")

        scroll_area = QScrollArea()
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff) 
        settings_tab.setCentralWidget(scroll_area)

        settings_widget = QWidget()
        scroll_area.setWidget(settings_widget)
        scroll_area.setWidgetResizable(True)

        self.addTab(settings_tab, "Settings")
        self.setCurrentIndex(self.count() - 1)

        toggle_proxy_button = QCheckBox("Allow proxy tor socks5://127.0.0.1:9050")
        toggle_proxy_button.setChecked(self.toggle_proxy_socks_state)
        toggle_proxy_button.stateChanged.connect(self.toggle_proxy)

        toggle_proxy_i2p_button = QCheckBox("Allow proxy i2p http://127.0.0.1:4444")
        toggle_proxy_i2p_button.setChecked(self.toggle_proxy_i2p_state)
        toggle_proxy_i2p_button.stateChanged.connect(self.toggle_i2p_proxy)

        toggle_proxy_i2p_https_button = QCheckBox("Allow proxy i2p https://127.0.0.1:4445")
        toggle_proxy_i2p_https_button.setChecked(self.toggle_proxy_i2p_https_state)
        toggle_proxy_i2p_https_button.stateChanged.connect(self.toggle_i2p_https_proxy)

        toggle_images_button = QCheckBox("Allow photos or gifs \nEnables image loading. It can generate unnecessary cache, in addition to making web pages load slower")
        toggle_images_button.setChecked(self.toggle_images_state)
        toggle_images_button.stateChanged.connect(self.toggle_images)

        toggle_javascript_button = QCheckBox("Allow JavaScript \nThe reason not to use JavaScript is to prevent sensitive information from being tracked or shared with third parties. \nJavaScript can be used to track users and collect information about them, such as IP addresses, location, browsing history, keystrokes, stored cookies, session tokens, or other sensitive information retained in the browser. \nFurthermore, there are other risks, such as websocket, which can be used for reverse remote access, XSS attacks (cross-site scripting) among others.")
        toggle_javascript_button.setChecked(self.toggle_javascript_state)
        toggle_javascript_button.stateChanged.connect(self.toggle_javascript)

        toggle_popup_button = QCheckBox("Allow PopUP \nAllows annoying messages that overlay the screen content, forcing you to pay attention to them")
        toggle_popup_button.setChecked(self.toggle_popup_state)
        toggle_popup_button.stateChanged.connect(self.toggle_popup)

        toggle_cookies_button = QCheckBox("Allow Cookies \nCookies are used to store data about you and your preferences. It is mainly used for personalized ads")
        toggle_cookies_button.setChecked(self.toggle_cookies_state)
        toggle_cookies_button.stateChanged.connect(self.toggle_cookies)

        toggle_error_button = QCheckBox("Allow Error Page \nThis setting controls whether the web view engine allows custom error pages to be displayed. \nBy setting this to False, you are disabling the display of custom error pages.")
        toggle_error_button.setChecked(self.toggle_error_state)
        toggle_error_button.stateChanged.connect(self.toggle_error)

        toggle_storage_button = QCheckBox("Allow Local Storage \nControls whether the web view engine allows the use of local storage (localStorage) in the browser. \nIf set to False, you are disabling the use of local storage.\n")
        toggle_storage_button.setChecked(self.toggle_storage_state)
        toggle_storage_button.stateChanged.connect(self.toggle_storage)

        toggle_content_button = QCheckBox("Allow Local Content Can Access Remote Urls \nControls whether local content can access remote resources. \nSetting this to False prevents local content from accessing resources on remote servers.\n")
        toggle_content_button.setChecked(self.toggle_content_state)
        toggle_content_button.stateChanged.connect(self.toggle_content)

        toggle_webgl_button = QCheckBox("Allow WebGL \nControls whether WebGL technology is enabled in the browser. \nDisabling this (False) means the browser will not support WebGL, which is used to render interactive 3D graphics on web pages.")
        toggle_webgl_button.setChecked(self.toggle_webgl_state)
        toggle_webgl_button.stateChanged.connect(self.toggle_webgl)

        toggle_plugins_button = QCheckBox("Allow Plugins \nControls whether plugins (such as Flash) are enabled in the browser. \nSetting it to False will disable plugins in the browser.\n")
        toggle_plugins_button.setChecked(self.toggle_plugins_state)
        toggle_plugins_button.stateChanged.connect(self.toggle_plugins)

        toggle_screen_button = QCheckBox("Allow Screen Capture \nControls whether screen capture of web content is allowed. \nSetting it to False will disable the ability to screenshot content displayed in the browser.")
        toggle_screen_button.setChecked(self.toggle_screen_state)
        toggle_screen_button.stateChanged.connect(self.toggle_screen)

        toggle_dns_prefetch_button = QCheckBox("Allow Dns Prefetch \nControls whether the browser will preload DNS resolutions. \nSetting it to False will disable preloading of DNS resolutions.")
        toggle_dns_prefetch_button.setChecked(self.toggle_dns_prefetch_state)
        toggle_dns_prefetch_button.stateChanged.connect(self.toggle_dns_prefetch)

        toggle_webrtc_button = QCheckBox("WebRTC Public Interfaces Only \nControls whether WebRTC (Web Real-Time Communication) communication should be restricted to public network interfaces or whether all network interfaces can be used. \nWhen this option is set to True, the web view engine will allow WebRTC communication to occur only through public network interfaces, which can be useful in security scenarios where you want to restrict WebRTC \nconnections to trusted network interfaces. and do not allow the use of local network interfaces. \nConversely, when this option is set to False, WebRTC communication can occur over all available network interfaces, including local and private interfaces.\n")
        toggle_webrtc_button.setChecked(self.toggle_webrtc_state)
        toggle_webrtc_button.stateChanged.connect(self.toggle_webrtc)

        toggle_disable_button = QCheckBox("Disable all")
        toggle_disable_button.setChecked(self.togle_disable_all_state)
        toggle_disable_button.stateChanged.connect(self.toggle_disable)

        toggle_enable_button = QCheckBox("Enable all")
        toggle_enable_button.setChecked(self.togle_enable_all_state)
        toggle_enable_button.stateChanged.connect(self.toggle_enable)

        toggle_check_button = QCheckBox("Footer Check")
        toggle_check_button.setChecked(self.toggle_check_state)
        toggle_check_button.stateChanged.connect(self.toggle_check)

        toggle_links_button = QCheckBox("Footer Links")
        toggle_links_button.setChecked(self.toggle_links_state)
        toggle_links_button.stateChanged.connect(self.toggle_links)

        toggle_download_confirmation_button = QCheckBox("Download confirmation")
        toggle_download_confirmation_button.setChecked(self.toggle_download_confirmation_state)
        toggle_download_confirmation_button.stateChanged.connect(self.toggle_download_confirmation)

        layout = QVBoxLayout()
        all_settings_label = QPushButton("All settings")
        all_settings_label.setStyleSheet("background-color: #000000; color: #0EAEF8; text-align: center; font-size: 28px; border: None;")
        layout.addWidget(all_settings_label)
        layout.addWidget(toggle_proxy_button)
        layout.addWidget(toggle_proxy_i2p_button)
        layout.addWidget(toggle_proxy_i2p_https_button)
        layout.addWidget(toggle_disable_button)
        layout.addWidget(toggle_enable_button)
        layout.addWidget(toggle_images_button)
        layout.addWidget(toggle_javascript_button)
        layout.addWidget(toggle_popup_button)
        layout.addWidget(toggle_cookies_button)
        layout.addWidget(toggle_error_button)
        layout.addWidget(toggle_storage_button)
        layout.addWidget(toggle_content_button)
        layout.addWidget(toggle_webgl_button)
        layout.addWidget(toggle_plugins_button)
        layout.addWidget(toggle_screen_button)
        layout.addWidget(toggle_dns_prefetch_button)
        layout.addWidget(toggle_webrtc_button)
        layout.addWidget(toggle_check_button)
        layout.addWidget(toggle_links_button)
        layout.addWidget(toggle_download_confirmation_button)

        exit_button = QPushButton("EXIT (Ctrl+z)")
        exit_button.setStyleSheet("background-color: #000000; color: red; font-size: 28px; border: None;")
        exit_button.clicked.connect(self.close_current_tab)
        layout.addWidget(exit_button)
        settings_widget.setLayout(layout)

    def create_historic_tab(self):
        for index in range(self.count()):
            if self.tabText(index) == "Historic":
                self.setCurrentIndex(index)
                return

        historic_tab = QMainWindow()
        historic_tab.setWindowTitle("Historic")

        scroll_area = QScrollArea()
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        historic_tab.setCentralWidget(scroll_area)

        historic_widget = QWidget()
        scroll_area.setWidget(historic_widget)
        scroll_area.setWidgetResizable(True)

        self.addTab(historic_tab, "Historic")
        self.setCurrentIndex(self.count() - 1)

        layout = QVBoxLayout()
        all_historic_label = QPushButton("All Historic")
        all_historic_label.setStyleSheet("background-color: #000000; color: #0EAEF8; text-align: center; font-size: 28px; border: None;")
        layout.addWidget(all_historic_label)

        button_layout = QHBoxLayout()

        clear_history_button = QPushButton("Clear history")
        clear_history_button.setStyleSheet("background-color: #000000; color: #ff0000; font-size: 18px; text-align: right; border: None;")
        clear_history_button.clicked.connect(self.clear_history)
        button_layout.addWidget(clear_history_button)

        reload_history_button = QPushButton("Reload")
        reload_history_button.setStyleSheet("background-color: #000000; color: #0EAEF8; font-size: 18px; text-align: left; border: None;")
        reload_history_button.clicked.connect(self.reload_history)
        button_layout.addWidget(reload_history_button)

        layout.addLayout(button_layout)
        historic_path = "/tmp/historic.md"

        if os.path.exists(historic_path):
            with open(historic_path, "r") as file:
                historic_entries = file.readlines()
                if historic_entries:
                    for entry in historic_entries:
                        entry_label = QLabel(entry.strip())
                        layout.addWidget(entry_label)
                else:
                    empty_label = QLabel("Empty history")
                    layout.addWidget(empty_label)
        else:
            empty_label = QLabel("Empty history")
            layout.addWidget(empty_label)

        exit_button = QPushButton("EXIT (Ctrl+z)")
        exit_button.setStyleSheet("background-color: #000000; color: #ff0000; font-size: 28px; border: None;")
        exit_button.clicked.connect(self.close_current_tab)
        layout.addWidget(exit_button)
        historic_widget.setLayout(layout)

    def clear_history(self):
        historic_path = "/tmp/historic.md"

        if os.path.exists(historic_path):
            with open(historic_path, "w") as file:
                file.write("")
                file.close()
            self.reload_history()
        else:
            pass

    def reload_history(self):
        self.close_current_tab()
        self.open_historic()

    def create_information_tab(self, state):
        self.toggle_proxy_socks_state = state
        proxies = None
        if args.proxy or state != False:
            proxies = {
                "https": "socks5h://127.0.0.1:9050",
                "http": "socks5h://127.0.0.1:9050"
            }

        headers = user_agent_
        info_dir = os.path.dirname(os.path.realpath(__file__))

        for index in range(self.count()):
            if self.tabText(index) == "Information":
                self.setCurrentIndex(index)
                return

        information_tab = QMainWindow()
        information_tab.setWindowTitle("Information")

        scroll_area = QScrollArea()
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        information_tab.setCentralWidget(scroll_area)

        information_widget = QWidget()
        scroll_area.setWidget(information_widget)
        scroll_area.setWidgetResizable(True)

        self.addTab(information_tab, "Information")
        self.setCurrentIndex(self.count() - 1)

        layout = QVBoxLayout()
        all_information_label = QPushButton("All Information")
        all_information_label.setStyleSheet("background-color: #000000; color: #0EAEF8; text-align: center; font-size: 28px; border: None;")
        layout.addWidget(all_information_label)

        def get_ip(self):
            theme_button_speed = "background-color: #000000; color: #0EAEF8; text-align: left; font-size: 18px; border: None; text-decoration: None;"
            subprocess.run(["python3.11", f"{info_dir}/QtWebEngine/Load/get_ip.py"], env={"PYTHONPATH": "."}, check=False)
            try:
                get_ip_address = GetIP()
                result = get_ip_address.Get_Ip(proxies=proxies, headers=headers)
                get_ip_button.setText(f"{result}")
                get_ip_button.setStyleSheet(theme_button_speed)
            except Exception as error:
                get_ip_button.setText(f"Error obtaining IP address: {error}")
                get_ip_button.setStyleSheet(theme_button_speed)

        def tor_check(self):
            theme_button_speed = "background-color: #000000; color: #0EAEF8; text-align: left; font-size: 18px; border: None; text-decoration: None;"
            subprocess.run(["python3.11", f"{info_dir}/QtWebEngine/Load/onion_check.py"], env={"PYTHONPATH": "."}, check=False)
            try:
                onion_c = OnionCheck()
                result = onion_c.is_onion(proxies=proxies, headers=headers)
                check_tor_button.setText(f"{result}")
                check_tor_button.setStyleSheet(theme_button_speed)
            except Exception as error:
                check_tor_button.setText(f"Error obtaining IP address: {error}")
                check_tor_button.setStyleSheet(theme_button_speed)

        def speed_check(self):
            theme_button_speed = "background-color: #000000; color: #0EAEF8; text-align: left; font-size: 18px; border: None; text-decoration: None;"
            subprocess.run(["python3.11", f"{info_dir}/QtWebEngine/Load/speed_test.py"], env={"PYTHONPATH": "."}, check=False)
            try:
                speed_test = SpeedTest()
                result = speed_test.speed_check(proxies=proxies, headers=headers)
                check_speed_button.setText(f"{result}")
                check_speed_button.setStyleSheet(theme_button_speed)
            except Exception as error:
                check_speed_button.setText("No successful speed tests were performed.")
                check_speed_button.setStyleSheet(theme_button_speed)

        def shortcuts_view(self):
            shortcuts_view_theme = "background-color: #000000; color: #0EAEF8; text-align: left; font-size: 18px; border: None; text-decoration: None;"
            shortcuts_button.setText(f"{shortcuts}")
            shortcuts_button.setStyleSheet(shortcuts_view_theme)

        theme_button_info = "background-color: #000000; color: #0EAEF8; text-align: left; font-size: 18px; border: None; text-decoration: underline;"
        get_ip_button = QPushButton("Get IP address")
        get_ip_button.setStyleSheet(theme_button_info)
        get_ip_button.clicked.connect(get_ip)
        layout.addWidget(get_ip_button)

        check_tor_button = QPushButton("Check tor")
        check_tor_button.setStyleSheet(theme_button_info)
        check_tor_button.clicked.connect(tor_check)
        layout.addWidget(check_tor_button)

        check_speed_button = QPushButton("Speedtest")
        check_speed_button.setStyleSheet(theme_button_info)
        check_speed_button.clicked.connect(speed_check)
        layout.addWidget(check_speed_button)

        shortcuts_button = QPushButton("Shortcuts")
        shortcuts_button.setStyleSheet(theme_button_info)
        shortcuts_button.clicked.connect(shortcuts_view)
        layout.addWidget(shortcuts_button)

        exit_button = QPushButton("EXIT (Ctrl+z)")
        exit_button.setStyleSheet("background-color: #000000; color: red; font-size: 28px; border: None;")
        exit_button.clicked.connect(self.close_current_tab)
        layout.addWidget(exit_button)
        information_widget.setLayout(layout)

    def toggle_images(self, state):
        self.toggle_images_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.AutoLoadImages, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_javascript(self, state):
        self.toggle_javascript_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, state)
            tab.browser.settings().setAttribute(QWebEngineSettings.JavascriptCanAccessClipboard, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_popup(self, state):
        self.toggle_popup_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_cookies(self, state):
        self.toggle_cookies_state = state
        if state:
            os.environ['QTWEBENGINE_COOKIE_STORE'] = '1'
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()
        else:
            os.environ['QTWEBENGINE_COOKIE_STORE'] = '0'
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_error(self, state):
        self.toggle_error_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.ErrorPageEnabled, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_storage(self, state):
        self.toggle_storage_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.LocalStorageEnabled, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_content(self, state):
        self.toggle_content_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.LocalContentCanAccessRemoteUrls, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_webgl(self, state):
        self.toggle_webgl_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.WebGLEnabled, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_plugins(self, state):
        self.toggle_plugins_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.PluginsEnabled, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_screen(self, state):
        self.toggle_screen_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.ScreenCaptureEnabled, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_dns_prefetch(self, state):
        self.toggle_dns_prefetch_state = state
        for tab in self.tabs:
            tab.browser.settings().setAttribute(QWebEngineSettings.DnsPrefetchEnabled, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_webrtc(self, state):
        self.toggle_webrtc_state = state
        for tab in self.tabs:
            tab.browser.page().profile().settings().setAttribute(QWebEngineSettings.WebRTCPublicInterfacesOnly, state)
            self.togle_enable_all_state = False
            self.togle_disable_all_state = False
#            self.reload_page()

    def toggle_proxy(self, state):
        self.toggle_proxy_socks_state = state
        if args.proxy or state != False:
            self.toggle_proxy_i2p_state = False
            self.toggle_proxy_i2p_https_state = False
            proxy = QNetworkProxy()
            if args.proxy:
                proxy_type, proxy_addr = args.proxy.split("://")
                proxy.setType(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.HttpProxy)
                proxy.setHostName(proxy_addr.split(":")[0])
                proxy.setPort(int(proxy_addr.split(":")[1]))
            else:
                proxy.setType(QNetworkProxy.Socks5Proxy)
                proxy.setHostName("127.0.0.1")
                proxy.setPort(9050)
            QNetworkProxy.setApplicationProxy(proxy)
        else:
# Disable proxy by setting an empty default proxy
            QNetworkProxy.setApplicationProxy(QNetworkProxy())
        self.reload_page()

    def toggle_i2p_proxy(self, state):
        self.toggle_proxy_i2p_state = state
        if args.proxy or state != False:
            self.toggle_proxy_socks_state = False
            self.toggle_proxy_i2p_https_state = False
            proxy = QNetworkProxy()
            if args.proxy:
                proxy_type, proxy_addr = args.proxy.split("://")
                proxy.setType(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.HttpProxy)
                proxy.setHostName(proxy_addr.split(":")[0])
                proxy.setPort(int(proxy_addr.split(":")[1]))
            else:
                proxy.setType(QNetworkProxy.HttpProxy)
                proxy.setHostName("127.0.0.1")
                proxy.setPort(4444)
            QNetworkProxy.setApplicationProxy(proxy)
        else:
# Disable proxy by setting an empty default proxy
            QNetworkProxy.setApplicationProxy(QNetworkProxy())
        self.reload_page()

    def toggle_i2p_https_proxy(self, state):
        self.toggle_proxy_i2p_https_state = state
        if args.proxy or state != False:
            self.toggle_proxy_socks_state = False
            self.toggle_proxy_i2p_state = False
            proxy = QNetworkProxy()
            if args.proxy:
                proxy_type, proxy_addr = args.proxy.split("://")
                proxy.setType(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.HttpProxy)
                proxy.setHostName(proxy_addr.split(":")[0])
                proxy.setPort(int(proxy_addr.split(":")[1]))
            else:
                proxy.setType(QNetworkProxy.HttpProxy)
                proxy.setHostName("127.0.0.1")
                proxy.setPort(4445)
            QNetworkProxy.setApplicationProxy(proxy)
        else:
# Disable proxy by setting an empty default proxy
            QNetworkProxy.setApplicationProxy(QNetworkProxy())
        self.reload_page()

    def toggle_disable(self, state):
        self.togle_disable_all_state = state
        if state != False:
            self.toggle_images_state = False
            self.toggle_javascript_state = False
            self.toggle_popup_state = False
            self.toggle_cookies_state = False
            self.toggle_error_state = False
            self.toggle_storage_state = False
            self.toggle_content_state = False
            self.toggle_webgl_state = False
            self.toggle_plugins_state = False
            self.toggle_screen_state = False
            self.toggle_dns_prefetch_state = False
            self.toggle_proxy_socks_state = False
            self.togle_enable_all_state = False
            self.reload_page()
        else:
            pass

    def toggle_enable(self, state):
        self.togle_enable_all_state = state
        if state != False:
            self.toggle_images_state = True
            self.toggle_javascript_state = True
            self.toggle_popup_state = True
            self.toggle_cookies_state = True
            self.toggle_error_state = True
            self.toggle_storage_state = True
            self.toggle_content_state = True
            self.toggle_webgl_state = True
            self.toggle_plugins_state = True
            self.toggle_screen_state = True
            self.toggle_dns_prefetch_state = True
            self.togle_disable_all_state = False
            self.reload_page()
        else:
            pass

    def toggle_check(self, state):
        self.toggle_check_state = state
        if state == False:
            for tab in self.tabs:
                tab.deactivate_footer_check()
        else:
            for tab in self.tabs:
                if self.toggle_links_state != False:
                    tab.deactivate_links_check()
                    self.toggle_links_state = False
                tab.toggle_footer_check(state)
            self.reload_page()

    def toggle_links(self, state):
        self.toggle_links_state = state
        if state == False:
            for tab in self.tabs:
                tab.deactivate_links_check()
        else:
            for tab in self.tabs:
                if self.toggle_check_state != False:
                    tab.deactivate_footer_check()
                    self.toggle_check_state = False
                tab.toggle_links_check(state)
            self.reload_page()

    def toggle_download_confirmation(self, state):
        self.toggle_download_confirmation_state = state
        if state != False:
            WebEngineView.confirm(self, state)
        else:
            WebEngineView.confirm(self, state)

    def reload_page(self):
        self.close_current_tab()
        self.open_settings()

    def exit_application(self):
        pass

    def main_signal(self):
        sys.exit()


class WebEngineView(QWebEngineView):
    show_progress_flag = False

    def __init__(self, parent=None, tabbed_browser=None):
        super().__init__(parent)
        self.page().profile().downloadRequested.connect(self.handle_download_requested)
        self.tabbed_browser = tabbed_browser
        self.page().newWindowRequested.connect(self.handle_new_window_requested)

        self.show_download_confirmation = False

    def confirm(self, state):
        if state != False:
            self.show_download_confirmation = True
        else:
            self.show_download_confirmation = False

    def handle_new_window_requested(self, web_engine_page):
        if self.tabbed_browser:
            new_tab = self.tabbed_browser.add_new_tab(args, web_engine_page.requestedUrl().toString())

    def handle_download_requested(self, download_item):
        download_url = download_item.url().toString()
        file_name = download_url.split("/")[-1]

        if args.download_directory:
            download_directory = args.download_directory
        else:
            download_directory = "/tmp"

        if self.tabbed_browser:
            self.tabbed_browser.set_download_name(file_name)

        if args.download_confirmation or self.show_download_confirmation == True:
# Creating the dialog box
            msg_box = QMessageBox(self)
            msg_box.setWindowTitle("Download Request")
            msg_box.setText(f"Do you want to download {file_name}?")

            accept_button = QPushButton("Accept")
            reject_button = QPushButton("Reject")

            msg_box.addButton(accept_button, QMessageBox.AcceptRole)
            msg_box.addButton(reject_button, QMessageBox.RejectRole)

            accept_button.clicked.connect(lambda: self.accept_download(download_url, file_name, download_directory))
            reject_button.clicked.connect(lambda: self.reject_download(file_name))

            result = msg_box.exec()
        else:
            self.accept_download(download_url, file_name, download_directory)

    def accept_download(self, download_url, file_name, download_directory):
# Called when the "Accept" button is clicked
        download_thread = threading.Thread(target=self.download_file, args=(download_url, file_name, download_directory))
        download_thread.start()

    def reject_download(self, file_name):
# Called when the "Reject" button is clicked
        if self.tabbed_browser:
            self.tabbed_browser.set_download_name(file_name, "Download rejected")

    def download_file(self, download_url, file_name, download_directory, progress_callback=None):
        try:
#            def progress_callback(progress):
#                if WebEngineView.show_progress_flag and self.tabbed_browser:
#                    self.tabbed_browser.set_download_progress(file_name, progress)
            DownloadManager.download_file(download_url, file_name, download_directory, progress_callback)
            if self.tabbed_browser:
                self.tabbed_browser.set_download_finished(file_name)
        except Exception as error:
            print(f"Error in download_file: {str(error)}")


class BrowserWindow(QMainWindow):
    def __init__(self, args, tabbed_browser=None):
        super().__init__(tabbed_browser)
        self.args = args
        self.tabbed_browser = tabbed_browser
        self.tabbed_browser.setWindowTitle(TITLE)
        self.showFullScreen()
        self.tabbed_browser.setGeometry(0, 0, QApplication.primaryScreen().availableGeometry().width(), QApplication.primaryScreen().availableGeometry().height())

# Set the window icon
#        icon = QIcon("browser.png")
#        self.setWindowIcon(icon)

# Creation of the scrollable widget (scroll area)
        self.scroll_area = QScrollArea()
        self.scroll_area.setWidgetResizable(True)

        self.title = "New Tab"
        self.tabbed_browser.setStyleSheet(css_style)

# Creation of QWebEngineView to display website content
        self.browser = WebEngineView(self, self.tabbed_browser)

# Configuring scrolling and setting QWebEngineView as internal widget
        self.scroll_area.setWidget(self.browser)

# Browser preferences directory
        if args.tempdir:
            temp_dir = tempfile.TemporaryDirectory()
            storage_path = os.path.join(temp_dir.name, "/tmp")
        else:
            current_directory = os.getcwd()
            storage_path = "/tmp/"

        webengine_data_path = os.path.join(storage_path, "Default")
        if not os.path.exists(webengine_data_path):
            os.makedirs(webengine_data_path)
        self.browser.page().profile().setPersistentStoragePath(webengine_data_path)

        self.browser.page().profile().setHttpUserAgent(user_agent)

# Variable to store the source code viewport
        self.source_window = None

        self.urlbar = QLineEdit()
        self.urlbar.returnPressed.connect(self.navigate_to_url)

        theme_set_button = "background-color: #000000; color: #ffffff; border: None; font-weight: bold;"
        theme_set_button_quit = "background-color: #000000; color: #ff0000; border: None; font-weight: bold;"
        theme_set_button_se = "background-color: #000000; color: #0EAEF8; border: None; font-weight: bold;"
        theme_set_button_tools = "background-color: #000000; color: #ffffff; border: None; font-weight: bold;"

        back_button = QPushButton("<")
        back_button.clicked.connect(self.navigate_back)
        back_button.setStyleSheet(theme_set_button)

        forward_button = QPushButton(">")
        forward_button.clicked.connect(self.navigate_forward)
        forward_button.setStyleSheet(theme_set_button)

        bookmarks_button = QPushButton("☰")
        bookmarks_button.clicked.connect(self.show_bookmarks)
        bookmarks_button.setStyleSheet(theme_set_button_tools)

        navigate_button = QPushButton("Go")
        navigate_button.clicked.connect(self.navigate_to_url)
        navigate_button.setStyleSheet(theme_set_button)

        self.search_engine_button = QPushButton("SE(surface)")
        self.search_engine_button.setStyleSheet(theme_set_button_se)
        self.create_search_engine_menu()
        self.search_engine_button.setMenu(self.navigate_menu)

        self.search_engine_onion_button = QPushButton("SE(tor)")
        self.search_engine_onion_button.setStyleSheet(theme_set_button_se)
        self.create_search_engine_menu_onion()
        self.search_engine_onion_button.setMenu(self.navigate_menu_onion)

        self.search_engine_i2p_button = QPushButton("SE(i2p)")
        self.search_engine_i2p_button.setStyleSheet(theme_set_button_se)
        self.create_search_engine_menu_i2p()
        self.search_engine_i2p_button.setMenu(self.navigate_menu_i2p)

        tools_menu = self.create_tools_menu()
        tools_button = QPushButton("⋮")
        tools_button.setStyleSheet(theme_set_button_tools)
        tools_button.setMenu(tools_menu)

        toolbar = QToolBar()
        self.addToolBar(toolbar)
        toolbar.addWidget(back_button)
        toolbar.addWidget(forward_button)
        toolbar.addWidget(bookmarks_button)
        toolbar.addWidget(self.urlbar)
        toolbar.addWidget(navigate_button)
        toolbar.addWidget(self.search_engine_button)
        toolbar.addWidget(self.search_engine_onion_button)
        toolbar.addWidget(self.search_engine_i2p_button)
        toolbar.addWidget(tools_button)

        layout = QVBoxLayout()
        layout.addWidget(self.scroll_area)

        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

# Bookmarks
        self.fixed_menu = QMenu("Pinned_bookmarks", self)
        self.fixed_menu.setStyleSheet("color: #00ff00;")
        for fixed_name in fixed:
            action = QAction(fixed_name, self)
            action.triggered.connect(functools.partial(self.open_fixed, fixed_name, tabbed_browser))
            self.fixed_menu.addAction(action)

        self.bookmark_menu = QMenu("Useful_Bookmarks", self)
        for bookmark_name in bookmarks:
            action = QAction(bookmark_name, self)
            action.triggered.connect(functools.partial(self.open_bookmark, bookmark_name, tabbed_browser))
            self.bookmark_menu.addAction(action)

        self.onion_menu = QMenu("Onion_bookmarks", self)
        for onion_name in onion:
            action = QAction(onion_name, self)
            action.triggered.connect(functools.partial(self.open_onion, onion_name, tabbed_browser))
            self.onion_menu.addAction(action)

        self.i2p_menu = QMenu("I2p_bookmarks", self)
        for i2p_name in i2p:
            action = QAction(i2p_name, self)
            action.triggered.connect(functools.partial(self.open_i2p, i2p_name, tabbed_browser))
            self.i2p_menu.addAction(action)

        self.toggle_bookmarks_state = True if args.bookmarks else False
        if args.bookmarks:
            self.menuBar().addMenu(self.fixed_menu)
            self.menuBar().addMenu(self.bookmark_menu)
            self.menuBar().addMenu(self.onion_menu)
            self.menuBar().addMenu(self.i2p_menu)

# Shortcuts
        rem_tab_shortcut = QShortcut(QKeySequence("Ctrl+R"), self)
        rem_tab_shortcut.activated.connect(self.close_current_tab_)

        add_tab_shortcut = QShortcut(QKeySequence("Ctrl+N"), self)
        add_tab_shortcut.activated.connect(self.add_new_tab)

        refresh_shortcut = QShortcut(QKeySequence("Ctrl+S"), self)
        refresh_shortcut.activated.connect(self.refresh_page)

        source_code_view = QShortcut(QKeySequence("Ctrl+U"), self)
        source_code_view.activated.connect(self.view_page_source)

        source_code_view = QShortcut(QKeySequence("Ctrl+Y"), self)
        source_code_view.activated.connect(self.view_page_source_in_new_window)

        close_source_view = QShortcut(QKeySequence("Ctrl+Z"), self)
        close_source_view.activated.connect(self.close_page_source_in_new_window)

        esc_shortcut = QShortcut(QKeySequence("Esc"), self)
        esc_shortcut.activated.connect(self.close_page_source_in_new_window)

        back_shortcut = QShortcut(QKeySequence(Qt.Key_Left), self)
        back_shortcut.activated.connect(self.navigate_back)

        forward_shortcut = QShortcut(QKeySequence(Qt.Key_Right), self)
        forward_shortcut.activated.connect(self.navigate_forward)

        self.browser.urlChanged.connect(self.update_urlbar)
        self.browser.loadFinished.connect(self.update_title)

        if args.footer_check and not args.footer_link:
            self.footer_check_()

        if args.footer_link and not args.footer_check:
            self.footer_link_()

        html_dir = os.path.dirname(os.path.realpath(__file__))
        homepage = f"{html_dir}/QtWebEngine/Load/index.html"
        url = args.url if args.url else f'file://{homepage}'
        if not args.url and not url.startswith('http://') and not url.startswith('https://') and not url.startswith('view-source:') and not url.startswith('file://'):
            url = 'https://' + url
        self.browser.setUrl(QUrl(url))

        selected_engine_url = ''
        print(selected_engine_url)

    def create_tools_menu(self):
        tools_menu = QMenu("Tools", self)

        settings_action = QAction("Settings", self)
        settings_action.triggered.connect(self.open_settings)
        tools_menu.addAction(settings_action)

        info_action = QAction("Informations", self)
        info_action.triggered.connect(self.open_informations)
        tools_menu.addAction(info_action)

        downloads_action = QAction("Downloads", self)
        downloads_action.triggered.connect(self.open_downloads_settings)
        tools_menu.addAction(downloads_action)

        historic_action = QAction("Historic", self)
        historic_action.triggered.connect(self.open_historic)
        tools_menu.addAction(historic_action)

        ua_action = QAction("User Agent", self)
        ua_action.triggered.connect(self.open_user_agent)
        tools_menu.addAction(ua_action)

        return tools_menu

    def open_user_agent(self):
        dialog = UserAgentDialog(self)
        result = dialog.exec()
        if result == QDialog.Accepted:
            selected_user_agent = dialog.get_selected_user_agent()
# Update user_agent in TabbedBrowser
            self.tabbed_browser.user_agent = selected_user_agent
# Configure the browser to use the User Agent selected in all Tabs
            for tab in self.tabbed_browser.tabs:
                tab.browser.page().profile().setHttpUserAgent(selected_user_agent)
                self.refresh_page

    def show_bookmarks(self):
        self.toggle_bookmarks_state = not self.toggle_bookmarks_state
        if self.toggle_bookmarks_state:
            self.menuBar().addMenu(self.fixed_menu)
            self.menuBar().addMenu(self.bookmark_menu)
            self.menuBar().addMenu(self.onion_menu)
            self.menuBar().addMenu(self.i2p_menu)
        else:
            self.menuBar().removeAction(self.fixed_menu.menuAction())
            self.menuBar().removeAction(self.bookmark_menu.menuAction())
            self.menuBar().removeAction(self.onion_menu.menuAction())
            self.menuBar().removeAction(self.i2p_menu.menuAction())

    def toggle_footer_check(self, state):
        if state != False:
            self.footer_check_()
        else:
            self.deactivate_footer_check()

    def toggle_links_check(self, state):
        if state != False:
            self.footer_link_()
        else:
            self.deactivate_links_check()

    def footer_check_(self):
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.central_layout = QVBoxLayout(self.central_widget)
        self.central_layout.addWidget(self.browser)
        self.status_label = QLabel()
        self.status_label.setStyleSheet("color: #0EAEF8; font-weight: bold; background-color: #000000; font-size: 10px;")
        self.status_label.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        self.central_layout.addWidget(self.status_label)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.auto_view_page_source_in_new_window)
        self.timer.start(5000)

    def deactivate_footer_check(self):
        self.status_label.setParent(None)
        self.timer.stop()

    def footer_link_(self):
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.browser.page().linkHovered.connect(self.show_link_in_status_bar)
        self.central_layout = QVBoxLayout(self.central_widget)
        self.central_layout.addWidget(self.browser)
        self.status_bar_label = QLabel()
        self.status_bar_label.setStyleSheet("color: #0EAEF8; font-weight: bold; background-color: #000000; font-size: 10px;")
        self.status_bar_label.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        self.central_layout.addWidget(self.status_bar_label)

    def deactivate_links_check(self):
        self.status_bar_label.setParent(None)

    def show_link_in_status_bar(self, link):
        self.status_bar_label.setText(link)

    def update_title(self):
        title = self.browser.page().title()
        self.title = title if title else "New Tab"
        self.setWindowTitle(self.title)

    def create_search_engine_menu(self):
        self.navigate_menu = QMenu("SE(surface)", self)

        for engine, prefix in SEARCH_ENGINES.items():
            action = self.navigate_menu.addAction(engine)
            action.triggered.connect(partial(self.handle_search_engine_selected, prefix))

        self.search_engine_button.setMenu(self.navigate_menu)
    selected_engine_url = ''

    def create_search_engine_menu_onion(self):
        self.navigate_menu_onion = QMenu("SE(tor)", self)

        for engine, prefix in SEARCH_ENGINES_ONION.items():
            action = self.navigate_menu_onion.addAction(engine)
            action.triggered.connect(partial(self.handle_search_engine_selected, prefix))

        self.search_engine_onion_button.setMenu(self.navigate_menu_onion)
    selected_engine_url = ''

    def create_search_engine_menu_i2p(self):
        self.navigate_menu_i2p = QMenu("SE(i2p)", self)

        for engine, prefix in SEARCH_ENGINES_I2P.items():
            action = self.navigate_menu_i2p.addAction(engine)
            action.triggered.connect(partial(self.handle_search_engine_selected, prefix))

        self.search_engine_i2p_button.setMenu(self.navigate_menu_i2p)
    selected_engine_url = ''

    def handle_search_engine_selected(self, prefix):
        self.selected_engine_url = prefix

    def update_urlbar(self, q):
        url = q.toString()

        if not url.startswith('http://') and not url.startswith('https://') and not url.startswith('view-source:') and not url.startswith('file://') and not url.startswith('ftp') and not url.startswith('ftps'):
            if re.search(r'\.\w{3}', url) and not ' ' in url and not ':' in url:
                url = f'https://{url}'
            elif not ' ' in url and ':' in url:
                url = f'http://{url}'
            else:
                search_query = url.replace(' ', '+')
                if self.selected_engine_url:
                    url = self.selected_engine_url + search_query
                else:
                    url = f'https://searx.nixnet.services/search?q={search_query}'

        self.urlbar.setText(url)
        self.urlbar.setCursorPosition(0)

        if url.startswith("https://"):
            self.urlbar.setStyleSheet("color: #00ff00;")
        elif url.startswith("http://"):
            self.urlbar.setStyleSheet("color: #ff0000;")
        elif url.startswith("view-source:"):
            self.urlbar.setStyleSheet("color: orange")
        else:
            self.urlbar.setStyleSheet("color: yellow;")

        self.save_url_to_history()

    def navigate_to_url(self):
        url = self.urlbar.text()
        if not url.startswith('http://') and not url.startswith('https://') and not url.startswith('view-source:') and not url.startswith('file://') and not url.startswith('ftp') and not url.startswith('ftps'):
            if re.search(r'\.\w{3}', url) and not ' ' in url and not ':' in url:
                url = f'https://{url}'
            elif not ' ' in url and ':' in url:
                url = f'http://{url}'
            else:
                search_query = url.replace(' ', '+')
                if self.selected_engine_url:
                    url = self.selected_engine_url + search_query
                else:
                    url = f'https://searx.nixnet.services/search?q={search_query}'

        q = QUrl(url)
        self.browser.setUrl(q)

    def save_url_to_history(self):
        current_url = self.browser.url().toString()

        if args.tempdir or args.no_historic:
            pass
        else:
            try:
                historic_path = "/tmp/historic.md"

                current_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                entry = f"{current_datetime} - {current_url}"

                with open(historic_path, "a") as file:
                    file.write(entry + "\n")
                    file.close()
            except:
                pass

    def close_current_tab_(self):
        current_tab_index = self.tabbed_browser.indexOf(self)
        if current_tab_index != -1:
            self.tabbed_browser.removeTab(current_tab_index)
            if current_tab_index == 0:
                if self.tabbed_browser.count() == 0:
                    self.browser.close()
                    app.quit()

    def add_new_tab(self):
        self.tabbed_browser.add_new_tab(args)

    def refresh_page(self):
        if self.browser.url():
            self.browser.reload()

    def navigate_back(self):
        if self.browser.history().canGoBack():
            self.browser.back()

    def navigate_forward(self):
        if self.browser.history().canGoForward():
            self.browser.forward()

    def open_downloads_settings(self):
        if self.tabbed_browser:
            self.tabbed_browser.open_downloads_settings()

    def open_settings(self):
        if self.tabbed_browser:
            self.tabbed_browser.open_settings()

    def open_historic(self):
        if self.tabbed_browser:
            self.tabbed_browser.open_historic()

    def open_informations(self):
        if self.tabbed_browser:
            state = self.tabbed_browser.toggle_proxy_socks_state
            self.tabbed_browser.open_informations(state)

    def view_page_source_in_new_window(self):
        if self.source_window is None:
            try:
                page_source = self.browser.page().toHtml(self.page_source_callback)
                self.source_window = QMainWindow()
                self.source_window.setWindowTitle("Page Source")
                self.source_window.setGeometry(100, 100, 800, 600)

                close_source_view = QShortcut(QKeySequence("Ctrl+Z"), self.source_window)
                close_source_view.activated.connect(self.close_page_source_in_new_window)

                esc_shortcut = QShortcut(QKeySequence("Esc"), self.source_window)
                esc_shortcut.activated.connect(self.close_page_source_in_new_window)

                self.source_window.show()
            except:
                self.source_window = None

    def auto_view_page_source_in_new_window(self):
        if self.source_window is None:
            def page_source_callback(html):
                if isinstance(html, str):
                    try:
                        script_count = html.count("<script")
                        onclick_count = html.count("onclick")
                        javascript_total = script_count + onclick_count
                        font_count = html.count('as="font"')
                        if script_count > 0 or onclick_count > 0 or font_count > 0:
                            self.status_label.setText(f"Remote fonts: ({font_count}), JavaScript: ({javascript_total})")
                            self.status_label.setStyleSheet("color: #ff0000; font-weight: bold; background-color: #000000; font-size: 10px;")
                        else:
                            self.status_label.setText("No Remote Fonts and JavaScript detected")
                            self.status_label.setStyleSheet("color: #00ff00; font-weight: bold; background-color: #000000; font-size: 10px; text-align: right;")
                    except:
                        self.source_window = None

                    self.source_window = None
            self.browser.page().toHtml(page_source_callback)

    def page_source_callback(self, page_source):
        text_edit = QTextEdit()
        text_edit.setPlainText(page_source)
        self.source_window.setCentralWidget(text_edit)

    def close_page_source_in_new_window(self):
        if self.source_window:
            self.source_window.close()
            self.source_window = None

    def view_page_source(self):
        current_url = self.browser.url()
        if current_url.isValid():
            source_url = QUrl("view-source:" + current_url.toString())
            self.tabbed_browser.add_new_tab(self.args, source_url.toString())
            self.tabbed_browser.create_new_tab(args)

    def open_bookmark(self, bookmark_name, tabbed_browser):
        if bookmark_name in bookmarks:
            url = bookmarks[bookmark_name]
            tabbed_browser.add_new_tab(args, url)
        else:
            print(f"Bookmark '{bookmark_name}' does not exist or not found.")

    def open_fixed(self, fixed_name, tabbed_browser):
        if fixed_name in fixed:
            url = fixed[fixed_name]
            tabbed_browser.add_new_tab(args, url)
        else:
            print(f"Pinned bookmark '{fixed_name}' does not exist or not found.")

    def open_onion(self, onion_name, tabbed_browser):
        if onion_name in onion:
            url = onion[onion_name]
            tabbed_browser.add_new_tab(args, url)
        else:
            print(f"Onion bookmark '{onion_name}' does not exist or not found.")

    def open_i2p(self, i2p_name, tabbed_browser):
        if i2p_name in i2p:
            url = i2p[i2p_name]
            tabbed_browser.add_new_tab(args, url)
        else:
            print(f"i2p bookmark '{i2p_name}' does not exist or not found.")

#Manage Your Extensions
    def addons_and_extensions(self):
        current_directory = os.getcwd()
        storage_path = os.path.join(current_directory, "QtWebEngine")
        if not os.path.exists(storage_path):
            os.makedirs(storage_path)
        addons_and_extensions_path = os.path.join(storage_path, "Addons")
        if not os.path.exists(addons_and_extensions_path):
            os.makedirs(addons_and_extensions_path)
#        print(addons_and_extensions_path)

    def close(self):
        self.browser.close()
        app.quit()


# Connect a signal handler for SIGINT (Ctrl+C)
def handle_sigint(signal, frame):
    app.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="A privacy-focused web browser, which uses the PySide6 module, has proxy support and by default has settings that protect browsing, even if it means sacrificing usability and breaking websites.",
        formatter_class=RawTextHelpFormatter,
        epilog="Examples:\n"
               f"{sys.argv[0]} --proxy socks5://127.0.0.1:9050 --url https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/\n"
               f"{sys.argv[0]} --proxy http://127.0.0.1:4444 --url http://gqt2klvr6r2hpdfxzt4bn2awwehsnc7l5w22fj3enbauxkhnzcoq.b32.i2p/\n"
               f"{sys.argv[0]} --enable_images --enable_javascript --enable_cookies\n"
               f"{sys.argv[0]} --download_directory /home/user/Downloads/\n"
               "\n"
               f"{shortcuts}"
    )
    parser.add_argument("--enable_images", action="store_true", help="Enables image loading. It can generate unnecessary cache, in addition to making web pages load slower")
    parser.add_argument("--enable_javascript", action="store_true", help="The reason not to use JavaScript is to prevent sensitive information from being tracked or shared with third parties. JavaScript can be used to track users and collect information about them, such as IP addresses, location, browsing history, keystrokes, stored cookies, session tokens, or other sensitive information retained in the browser. Furthermore, there are other risks, such as websocket, which can be used for reverse remote access, XSS attacks (cross-site scripting) among others.")
    parser.add_argument("--enable_popup", action="store_true", help="Allows annoying messages that overlay the screen content, forcing you to pay attention to them")
    parser.add_argument("--enable_cookies", action="store_true", help="Cookies are used to store data about you and your preferences. It is mainly used for personalized ads")
    parser.add_argument("--proxy", help="socks5://127.0.0.1:9050 http://127.0.0.1:4444 https://127.0.0.1:4445")
    parser.add_argument("--url", help="URL to open in the browser.")
    parser.add_argument("--download_directory", help="Directory to save downloads")
    parser.add_argument("--enable_error", action="store_true", help="This setting controls whether the web view engine allows custom error pages to be displayed. By setting this to False, you are disabling the display of custom error pages.")
    parser.add_argument("--enable_local_storage", action="store_true", help="Controls whether the web view engine allows the use of local storage (localStorage) in the browser. If set to False, you are disabling the use of local storage.")
    parser.add_argument("--enable_local_remote_acess", action="store_true", help="Controls whether local content can access remote resources. Setting this to False prevents local content from accessing resources on remote servers.")
    parser.add_argument("--enable_webgl", action="store_true", help="Controls whether WebGL technology is enabled in the browser. Disabling this (False) means the browser will not support WebGL, which is used to render interactive 3D graphics on web pages.")
    parser.add_argument("--enable_plugins", action="store_true", help="Controls whether plugins (such as Flash) are enabled in the browser. Setting it to False will disable plugins in the browser.")
    parser.add_argument("--enable_screen_capture", action="store_true", help="Controls whether screen capture of web content is allowed. Setting it to False will disable the ability to screenshot content displayed in the browser.")
    parser.add_argument("--enable_dns_prefetch", action="store_true", help="Controls whether the browser will preload DNS resolutions. Setting it to False will disable preloading of DNS resolutions.")
    parser.add_argument("--disable_webrtc", action="store_true", help="Controls whether WebRTC (Web Real-Time Communication) communication should be restricted to public network interfaces or whether all network interfaces can be used. When this option is set to True, the web view engine will allow WebRTC communication to occur only through public network interfaces, which can be useful in security scenarios where you want to restrict WebRTC connections to trusted network interfaces. and do not allow the use of local network interfaces. Conversely, when this option is set to False, WebRTC communication can occur over all available network interfaces, including local and private interfaces.")
    parser.add_argument("--only_verified_certificates", action="store_true", help="Only allow certificates issued by a certificate authority. Although self-generated certificates are recognized as possibly dangerous, this hides the fact that the only purpose of requiring certificate authority certificates is to prevent anonymity. When activating this option, javascript and error page will be enabled, especially because those who prefer certificates from authorities are not concerned about privacy.")
    parser.add_argument("--footer_link", action="store_true", help="Enable the footer with links, where, when you hover the mouse over a link, it will be shown there")
    parser.add_argument("--footer_check", action="store_true", help="Enable the footer with javascript checking and third-party sourcesi")
    parser.add_argument("--tempdir", action="store_true", help="With this option, no information such as your browsing history, cookies and preferences will be stored.")
    parser.add_argument("--no_historic", action="store_true", help="With this option, your browsing history will not be stored.")
    parser.add_argument("--download_confirmation", action="store_true", help="When making a download, choose whether you want to see a warning message before accepting the order. Not using this option causes downloads to be carried out automatically, without confirmation.")
    parser.add_argument("--bookmarks", action="store_true", help="Activates bookmarks in all Tabs. You can also do this later directly in the browser.")
    parser.add_argument("--no_sandbox", action="store_true", help="This option will disable sandboxing. Use as needed")
    args = parser.parse_args()

    app = QApplication(sys.argv)
#    app.setStyleSheet(load_stylesheet())

    os.environ['QTWEBENGINE_ENABLE_LINUX_ACCESSIBILITY'] = '0'
    os.environ['QTWEBENGINE_CHROMIUM_FLAGS'] = '--sandbox'
    os.environ['QTWEBENGINE_HIDE_SCROLLBARS'] = '1'
    os.environ['QTWEBENGINE_CHROMIUM_FLAGS'] = '--force-color-profile=srgb()'
    os.environ['QTWEBENGINE_CHROMIUM_FLAGS'] = '--blink-settings=darkMode=4,darkModeImagePolicy=2'

# Disable sandbox
    if args.no_sandbox:
        os.environ['QTWEBENGINE_DISABLE_SANDBOX'] = '1'

# Set environment variables to disable certificate checking
    if args.only_verified_certificates:
        pass
    else:
        os.environ['QTWEBENGINE_CHROMIUM_FLAGS'] = '--ignore-certificate-errors'

# Create the main browser window
    window = TabbedBrowser(args)
    window.show()

# Apply the style to the main window
    window.setStyleSheet(css_style)

    signal.signal(signal.SIGINT, handle_sigint)

# Create shortcuts to use in the application
    quit_shortcut = QShortcut(QKeySequence("Ctrl+Q"), window)
    quit_shortcut.activated.connect(app.quit)

    quit_shortcut_x = QShortcut(QKeySequence("Ctrl+X"), window)
    quit_shortcut_x.activated.connect(app.quit)

#    sys.exit(app.exec())

    BROWSER_START = threading.Thread(target=sys.exit(app.exec()))
    BROWSER_START.start()

