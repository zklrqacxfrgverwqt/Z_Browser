#!/usr/bin/env python3.11

import sys
import time
try:
    import requests
except ImportError:
    sys.exit("Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user requests")
#Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user pysocks"

class SpeedTest:
    @staticmethod
    def speed_check(proxies=None, headers=None):
        sites = [
            "https://duckduckgo.com",
        ]

        download_speeds = []

        try:
            for site in sites:
                try:
                    start_time = time.time()
                    response = requests.get(site, proxies=proxies, headers=headers)
                    end_time = time.time()

                    if response.status_code == 200:
                        download_speed = len(response.content) / (end_time - start_time) / 1024 / 1024  # Mbps
                        download_speeds.append(download_speed)
                    else:
                        return f"Failed to download from {site}. Status code: {response.status_code}"
                except requests.exceptions.RequestException as error:
                    return "Failed to establish a connection to {site}: {str(error)}"

            if download_speeds:
                average_speed = sum(download_speeds) / len(download_speeds)
                return f"Average Download Speed: {average_speed:.2f} Mbps"
            else:
                return "No successful speed tests were performed."
        except Exception as error:
            return f"Error in speed check: {error}."
            sys.exit()
