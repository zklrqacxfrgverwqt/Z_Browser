#!/usr/bin/env python3.11

SEARCH_ENGINES_I2P = {
    'Duckduckgo': 'http://gqt2klvr6r2hpdfxzt4bn2awwehsnc7l5w22fj3enbauxkhnzcoq.b32.i2p/?q=',
    'Mojeek': 'http://zc347qqc4pvo7lf73rhuhthd3i3g2v5zgnsqdoklexx2bcau7rgq.b32.i2p/search?date=1&size=1&t=40&q=',
}
