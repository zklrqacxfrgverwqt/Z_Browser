#!/usr/bin/env python3.11

import sys
import time
try:
    import requests
except ImportError:
    sys.exit("Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user requests")
#Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user pysocks"

class OnionCheck:
    @staticmethod
    def is_onion(proxies=None, headers=None):
        link = "https://check.torproject.org"
        try:
            response = requests.get(link, headers=headers, proxies=proxies)
            if response.status_code == 200:
                link_check = response.text.split('\n')
                status_line = link_check[7]
                return f"Result: {status_line}"
            else:
                return "Failed to check IP address"

        except Exception as error:
            return f"Error: {error}"
            sys.exit()
