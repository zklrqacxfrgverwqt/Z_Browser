#!/usr/bin/env python3.11

import sys
import time
try:
    import requests
except ImportError:
    sys.exit("Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user requests")
#Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user pysocks"

class GetIP:
    @staticmethod
    def Get_Ip(proxies=None, headers=None):
        link = "https://api.ipify.org"
        try:
            response = requests.get(link, headers=headers, proxies=proxies)
            if response.status_code == 200:
                ip_address = response.text
                return f"IP Address: {ip_address}"
            else:
                return "Failed to obtain IP address"
        except Exception as error:
            return f"Error obtaining IP address: {error}"
