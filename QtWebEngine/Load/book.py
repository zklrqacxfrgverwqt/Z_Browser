#!/usr/bin/env python3.11

fixed = {
    'ChatGPT': 'https://openai.com/chatgpt',
    'DuckDuckGo': 'https://duckduckgo.com',
    'DuckDuckGo html': 'https://html.duckduckgo.com/html',
    'Google': 'https://google.com',
    'Invidious': 'https://invidious.flokinet.to/feed/popular',
    'YouTube': 'https://www.youtube.com/',
}

bookmarks = {
    'Debian Download': 'http://debian.c3sl.ufpr.br/debian-cd/',
    'DNS Leak': 'https://www.dnsleaktest.com/',
    'Fingerprint Information': 'https://www.deviceinfo.me/',
    'I2p download': 'https://geti2p.net/en/download',
    'Kali Linux': 'https://www.kali.org/get-kali/#kali-platforms',
    'Nginx download': 'https://nginx.org/download/',
    'Translate projectsegfau': 'https://translate.projectsegfau.lt/',
}

onion = {
    'Invidious': 'http://grwp24hodrefzvjjuccrkw3mjq4tzhaaq32amf33dzpmuxe7ilepcmad.onion/feed/popular',
    'Respostas Ocultas': 'http://xh6liiypqffzwnu5734ucwps37tn2g6npthvugz3gdoqpikujju525yd.onion/',
    'Sanatório': 'http://i346ynyrc4wo3aatdzacvqgrry7ew6k6ziq26gqgwbubah4ohlqhz7id.onion/',
    'Tor Project Onion': 'http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion/index.html',
    'Tutoriais': 'http://xh6liiypqffzwnu5734ucwps37tn2g6npthvugz3gdoqpikujju525yd.onion/index.php/p%C3%A1gina-links#tutoriais',
}

i2p = {
    'Acetone\'s monero full node': 'http://moneropqhxs7jbsp3u7zqa7khb2gvfdr52g3fxnhk5samaxbh4na.b32.i2p/',
    'Bandura': 'http://bandura.i2p/eepsites/',
    'Identiguy': 'http://identiguy.i2p/',
    'I2P/Onion Monero Blockchain Explorer': 'http://je4vbduigce7pgjivvo26d4abn3u5mfsos5zemp3a75xzcv7nm4q.b32.i2p/',
    'MMGen Wallet': 'http://xi4leavi7voe336oosqzjai6s3qvc4g6uaxks72c3lqhnswngxcq.b32.i2p/',
    'Notbob (links)': 'http://notbob.i2p/links.html',
    'OpenBSD': 'http://ahwlj76krc5do77pibighi2frqmfccpezyiqfmiernvivwomgvuq.b32.i2p/',
    'Ramble Forum': 'http://ramble.i2p/',
    'Site oficial i2p': 'http://i2p-projekt.i2p/en/',
    'Stack Wallet': 'http://iznb6nejvt4cxbs6k7a3p2kgmctparxlbn6ewwml23iqq2gob5ba.b32.i2p/',
    'Stack Wallet': 'http://mituupybd7l7kloh4tblgkapumlvny7ujyiluzodibcrhrcp2fha.b32.i2p/',
    'The Monero Archive': 'http://khplhtfsbnzitlmj3lmdl7m36ale7iuf2kljvfmomvn7uwrvqx7a.b32.i2p/',
}

shortcuts = f'''
Shortcuts:
    ctrl+q                Close the browser
    ctrl+x                Close the browser
    ctrl+u                View page source
    ctrl+y                View page source in new window
    ctrl+z                Close page source in new window
    esc                   Close page source in new window
    ctrl+Key_Left         Back_shortcut  
    ctrl+Key_Right        Forward_shortcut
    ctrl+r                Rem tab shortcut
    ctrl+n                Add tab shortcut
    ctrl+s                Refresh shortcut
'''

user_agent = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
user_agent_ = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'}
