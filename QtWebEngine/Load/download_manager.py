#!/usr/bin/env python3.11

import sys
try:
    import requests
except ImportError:
    sys.exit("Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user requests")
#Please install missing librarys or dependencies: python3.11 -m pip install --break-system-package --user pysocks"

class DownloadManager:
    @staticmethod
    def download_file(download_url, file_name, download_directory, progress_callback=None):
        try:
            with open(f'{download_directory}/{file_name}', 'wb') as file:
                response = requests.get(download_url, stream=True)
                total_size = int(response.headers.get('content-length'))
                downloaded = 0

                for data in response.iter_content(chunk_size=4096):
                    file.write(data)
                    downloaded += len(data)

#                    if progress_callback:
# Convert sizes to more friendly units
#                        downloaded_kb = downloaded / 1024
#                        downloaded_mb = downloaded_kb / 1024
#                        downloaded_gb = downloaded_mb / 1024

# Call the progress callback with sizes
#                        progress_callback(downloaded_mb)

#                    if total_size > 0:
#                        progress = int(100 * downloaded / total_size)
#                        if progress_callback:
#                            progress_callback(progress)

        except Exception as error:
            print(f"Error downloading {file_name}: {str(error)}")
